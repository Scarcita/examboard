import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Kanban from '../board/Kanban'

export default function Home() {
  return (
    <div>
      <Kanban/>
    </div>
  )
}
